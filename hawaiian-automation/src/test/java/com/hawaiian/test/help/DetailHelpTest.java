package com.hawaiian.test.help;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.hawaiian.pages.help.HelpCenterPage;
import com.hawaiian.test.base.HawaiianBaseTest;

//import com.hawaiian.test.base.HawaiianBaseTest;

public class DetailHelpTest extends HawaiianBaseTest {
	public static final Logger logger = LogManager.getLogger(DetailHelpTest.class.getName());

	public By AirportInfo = By.id("Summary_450");
	public By CheckIn = By.id("Summary_464");
	public By HawaiianMiles = By.id("Summary_461");
	public By HelpCenterTab = By.xpath("//a[contains(text(),'Help Center')]");
	public By HelpLogo = By.xpath("//h1[contains(text(),'Help Center')]");

	@Test(priority = 1, groups = { "sanity", "regression", "help", "detailhelptest" })
	public void logoTest() {
		logger.info("Executing logo test");
		driver.findElement(HelpCenterTab).click();
		assertTrue(isElementPresent(HelpLogo), "Logo not found");
		logger.info("verifying logo test is passed");
	}

	@Test(priority = 2, groups = { "regression", "help", "detailhelptest" })
	public void infoTest() throws InterruptedException {
		logger.info("Executing info test");
		driver.findElement(AirportInfo).click();
		// Thread.sleep(5000);
		driver.findElement(HelpCenterTab).click();
		logger.info("verify info test is passed");
		Thread.sleep(5000);

	}

	@Test(priority = 3, groups = { "regression", "help", "detailhelptest" })
	public void checkInDetailTest() throws InterruptedException {
		logger.info("Executing checkIndetail test");
		driver.findElement(CheckIn).click();
		// Thread.sleep(5000);
		driver.findElement(HelpCenterTab).click();
		logger.info("verify checkindetail is passed ");
		Thread.sleep(5000);
	}

	@Test(priority = 4)
	public void milesTest() throws InterruptedException {
		logger.info("Executing miles Test");
		driver.findElement(HawaiianMiles).click();
		// Thread.sleep(5000);
		driver.findElement(HelpCenterTab).click();
		logger.info("verify miles test is passed");
		Thread.sleep(5000);

	}
}
