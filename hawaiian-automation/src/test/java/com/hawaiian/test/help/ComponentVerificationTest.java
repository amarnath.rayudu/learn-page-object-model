package com.hawaiian.test.help;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.hawaiian.test.base.HawaiianBaseTest;

//import com.hawaiian.test.base.HawaiianBaseTest;

public class ComponentVerificationTest extends HawaiianBaseTest {
	public By AirportInfo = By.id("Summary_450");
	public By CheckIn = By.id("Summary_464");
	public By HawaiianMiles = By.id("Summary_461");
	public By AirportGuides = By.xpath("//div[contains(text(),'guides')]");
	public By AtTheAirport = By.xpath("//div[contains(text(),'At')]");
	public By AccountMgt = By.xpath("//div[contains(text(),'Acco')]");
	public By HelpCenterTab = By.xpath("//a[contains(text(),'Help Center')]");
	public By HelpLogo = By.xpath("//h1[contains(text(),'Help Center')]");
	public static final Logger logger = LogManager.getLogger(ComponentVerificationTest.class.getName());
	@Test(priority = 1, groups={"sanity","regression","help","componentverification"})
	public void logoVerificationTest() throws InterruptedException {
		driver.findElement(HelpCenterTab).click();
		Thread.sleep(5000);
		logger.info("verifying HelpLogo");
		assertTrue(isElementPresent(HelpLogo), "Logo not found");

	}

	@Test(priority = 2,groups={"regression","help","componentverification"})
	public void airportComponentTest() throws InterruptedException {
		driver.findElement(AirportInfo).click();
		// Thread.sleep(5000);
		logger.info("verifying AirportGuides");
		assertTrue(isElementPresent(AirportGuides), "Airport guides not present");
		driver.findElement(HelpCenterTab).click();
		Thread.sleep(5000);
	}

	@Test(priority = 3,groups={"regression","help","componentverification"})
	public void checkInComponentTest() throws InterruptedException {
		logger.info("clik on  CheckIn link");
		driver.findElement(CheckIn).click();
		logger.info(" verifying AtTheAirport");
		assertTrue(isElementPresent(AtTheAirport), "At the airport not present");
		driver.findElement(HelpCenterTab).click();
		Thread.sleep(5000);
	}

	@Test(priority = 4,groups={"regression","help","componentverification"})
	public void hawaiianMilesComponentTest() throws InterruptedException {
		logger.info(" verifying HawaiianMiles link");
		driver.findElement(HawaiianMiles).click();
		logger.info(" verifying AccountMgt ");
		assertTrue(isElementPresent(AccountMgt), "Account management not present");
		driver.findElement(HelpCenterTab).click();
		Thread.sleep(5000);
	}
}
