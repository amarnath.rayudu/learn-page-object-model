package com.hawaiian.test.help;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.hawaiian.pages.help.HelpCenterPage;
import com.hawaiian.pages.home.HomePage;
import com.hawaiian.test.base.HawaiianBaseTest;

public class HelpHomeTest extends HawaiianBaseTest {
	HomePage homePage;
	HelpCenterPage hcp;
	public static final Logger logger = LogManager.getLogger(HelpHomeTest.class.getName());

	@Test(groups={"sanity","regression","help","helphome"})
	public void verifyHomePageTest() {
		logger.info(" Executing verifyHomePageTest ");
		homePage = new HomePage();
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
		logger.info(" verifyHomePageTest is passed");
	}

	@Test(dependsOnMethods = "verifyHomePageTest",groups={"regression","help","helphome"})
	public void navigateToHelpPageTest() {
		logger.info(" Executing navigateToHelpPageTest ");
		hcp = homePage.navigateToHelpCenter();
		assertTrue(hcp.isHelpCenterPageLoaded(), "Help center page is not laoded");
		logger.info(" navigateToHelpPageTest is passed");
	}

	@Test(dependsOnMethods = "navigateToHelpPageTest",groups={"regression","help","helphome"})
	public void checkInTest() {
		logger.info(" Executing checkInTest ");
		assertTrue(hcp.isCheckinLinkPrsent(), "checkin not present");
		logger.info(" checkInTest is passed");
	}

	@Test(dependsOnMethods = "checkInTest",groups={"regression","help","helphome"})
	public void hawaiianMilesVerificationTest() {
		logger.info(" Executing hawaiianMilesVerificationTest ");
		assertTrue(hcp.isMilesLinkPresent(), "checkin not present");
		logger.info(" hawaiianMilesVerificationTest is passed");

	}

	@Test(dependsOnMethods = "hawaiianMilesVerificationTest",groups={"regression","help","helphome"})
	public void navigateToHomeTest() {
		logger.info(" Executing navigateToHomeTest ");
		homePage = hcp.navigateToHome();
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
		logger.info(" navigateToHomeTest is passed");

	}

}
