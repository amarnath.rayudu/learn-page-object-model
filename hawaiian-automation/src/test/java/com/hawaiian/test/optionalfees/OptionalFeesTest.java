package com.hawaiian.test.optionalfees;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.hawaiian.page.optionalfees.OptionalFeesPage;
import com.hawaiian.pages.home.HomePage;
import com.hawaiian.test.base.HawaiianBaseTest;

public class OptionalFeesTest extends HawaiianBaseTest {
	HomePage homePage;
	OptionalFeesPage optionalFeesPage;
	public static final Logger logger = LogManager.getLogger(OptionalFeesTest.class.getName());
	@Test
	public void verifyHomePageTest() {
		logger.info("Executing verifyHomePageTest");
		homePage = new HomePage();
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
		logger.info("verifyHomepageTest is passed");
	}

	@Test(dependsOnMethods = "verifyHomePageTest")
	public void navigateToOptionalFeesPageTest(){
		logger.info("Executing verify navigateToOptionalFeesPageTest");
		optionalFeesPage = homePage.navigateToOptionalFees();
		assertTrue(optionalFeesPage.isOptionalFeesPageLoaded(), "optional fees  page not loaded");
		logger.info("navigateToOptionalFeesPageTest is passed");
	}

	@Test(dependsOnMethods = "navigateToOptionalFeesPageTest")
	public void baggageTest() {
		logger.info("Executing baggageTest");
		assertTrue(optionalFeesPage.isbaggageTabPresent(), "baggage tab not present");
		logger.info("baggageTest is passed");
	}

	@Test(dependsOnMethods = "baggageTest")
	public void legalTest() {
		logger.info("Executing legalTest");
		assertTrue(optionalFeesPage.islegalTabPresent(), "legal tab not present");
		logger.info("legalTest is passed");
	}

	@Test(dependsOnMethods = "legalTest")
	public void kiteTest() {
		logger.info("Executing kiteTest");
		assertTrue(optionalFeesPage.iskiteTabPresent(), "kite tab not present");
		logger.info("kiteTest is Passed");
	}

	@Test(dependsOnMethods = "kiteTest")
	public void navigateToHomeTest() {
		logger.info("Executing navigateToHome");
		homePage = optionalFeesPage.navigateToHome();
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
		logger.info("navigateToHomeTest is passed");
	}

}
