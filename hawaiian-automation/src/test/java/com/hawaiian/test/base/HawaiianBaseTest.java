package com.hawaiian.test.base;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.hawaiian.pages.HawaiianBasePage;
import com.hawaiian.utils.PropertyReader;

public class HawaiianBaseTest {
	public static WebDriver driver;
	public PropertyReader propertyReader = new PropertyReader();
	String url = "https://www.hawaiianairlines.com/";
	public static final Logger logger = LogManager.getLogger(HawaiianBaseTest.class.getName());

	@BeforeSuite
	public void init() {
		String browser = propertyReader.getProperty("browser");
		logger.info(" Browser provided in the properties is --- " + browser);
		if (browser.equals("chrome")) {
			logger.info(" Loading the driver for --- " + browser);

			System.setProperty("webdriver.chrome.driver", propertyReader.getProperty("chromepath"));
			driver = new ChromeDriver();
		} else {
			logger.info(" Loading the driver for --- " + browser);
			System.setProperty("webdriver.gecko.driver", propertyReader.getProperty("firefoxpath"));
			driver = new FirefoxDriver();
		}
		HawaiianBasePage.driver = driver;
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get(url);

	}

	@AfterSuite
	public void teardown() {
		logger.info(" Execution is completed ");
		driver.quit();
	}

	public boolean isElementPresent(By locator) {
		logger.info(" Verifying the presence of element " + locator);
		boolean isPresent = driver.findElements(locator).size() > 0;
		return isPresent;
	}

}
