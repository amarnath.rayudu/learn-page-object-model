package com.hawaiian.test.contactus;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.hawaiian.page.contactus.CallUsPage;
import com.hawaiian.page.contactus.ContactUsPage;
import com.hawaiian.pages.home.HomePage;
import com.hawaiian.test.base.HawaiianBaseTest;

public class CallUsBasicTest extends HawaiianBaseTest {
	public static final Logger logger = LogManager.getLogger(CallUsBasicTest.class.getName());
	HomePage homePage;
	ContactUsPage contactUsPage;
	CallUsPage callUsPage;

	@Test(groups = { "sanity", "regression", "contactus", "callus" })
	public void verifyHomePageTest() {
		homePage = new HomePage();
		logger.info("Executing verifyHomePageTest");
		assertTrue(homePage.isHomePageLoaded(), "Homepage is not loaded");
		logger.info("verify HomepageTest is passed");
	}

	@Test(dependsOnMethods = "verifyHomePageTest", groups = { "regression", "contactus", "callus" })
	public void navigateToContactUsPageTest() {
		contactUsPage = homePage.navigateToContactUs();
		logger.info("Executing navigate to contactuspageTest");
		assertTrue(contactUsPage.isContactUsPageloaded(), "Contactus page not loaded");
		logger.info("verify navigate to contactuspage is passed");

	}

	@Test(dependsOnMethods = "navigateToContactUsPageTest", groups = { "regression", "contactus", "callus" })
	public void navigateToCallUsPageTest() {
		callUsPage = contactUsPage.navigateToCallUsPage();
		logger.info("Executing navigate to callUsPageTest");
		assertTrue(callUsPage.isCallUsPageloaded(), "CallUs Page is not loaded");
		logger.info("verify navigate to callUsPage is passed");
	}

	@Test(dependsOnMethods = "navigateToCallUsPageTest", groups = { "regression", "contactus", "callus" })
	public void verifysUSTest() {
		logger.info("Executing verifyCallUsTest");
		assertTrue(callUsPage.isUSAnsCanadaPresent(), "US and Canada is not there");
		logger.info("Verify Us and Canada is Present");
	}
}
