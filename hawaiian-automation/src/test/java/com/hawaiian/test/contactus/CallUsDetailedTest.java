package com.hawaiian.test.contactus;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.hawaiian.page.contactus.CallUsPage;
import com.hawaiian.page.contactus.ContactUsPage;
import com.hawaiian.pages.home.HomePage;
import com.hawaiian.test.base.HawaiianBaseTest;

public class CallUsDetailedTest extends HawaiianBaseTest {
	public static final Logger logger = LogManager.getLogger(CallUsDetailedTest.class.getName());
	HomePage homePage;
	ContactUsPage contactUsPage;
	CallUsPage callUsPage;

	@Test(groups = { "sanity", "regression", "contactus", "callusdetailed" })
	public void navigateToCallUsPageTest() {
		logger.info("Executing navigate to CallUs Page Test");
		homePage = new HomePage();
		assertTrue(homePage.isHomePageLoaded(), "Homepage is not loaded");
		logger.info("Verify HomePage is loaded");
		contactUsPage = homePage.navigateToContactUs();
		logger.info("Executing navigate to contactuspageTest");
		assertTrue(contactUsPage.isContactUsPageloaded(), "Contactus page not loaded");
		logger.info("verify navigate to contactuspage is passed");
		callUsPage = contactUsPage.navigateToCallUsPage();
		logger.info("Executing navigate to callUsPageTest");
		assertTrue(callUsPage.isCallUsPageloaded(), "CallUs Page is not loaded");
		logger.info("verify navigate to callUsPage is passed");
	}

	@Test(dependsOnMethods = "navigateToCallUsPageTest", groups = { "regression", "contactus", "callusdetailed" })
	public void expeandUSTest() {
		logger.info("Executing expand US Test");
		assertTrue(callUsPage.isUsTollFreeNumberPresent(), "Us Toll number is not there");
		logger.info("Verify Us Toll numberPresent test is passed");
	}

	@Test(dependsOnMethods = "expeandUSTest", groups = { "regression", "contactus", "callusdetailed" })
	public void verifyChinaTest() {
		logger.info("Execuitng verifyChinaTest");
		assertTrue(callUsPage.isChinaPresent(), "China is not Present");
		logger.info("Verify isChinaPresent test is Passed");
	}

	@Test(dependsOnMethods = "verifyChinaTest", groups = { "regression", "contactus", "callusdetailed" })
	public void verifyUkTest() {
		logger.info("Executing verifyUkTest");
		assertTrue(callUsPage.isUnitedKingdomPresent(), "United Kingdom is not there");
		logger.info("Verify isUK Test is Passed");
	}

}
