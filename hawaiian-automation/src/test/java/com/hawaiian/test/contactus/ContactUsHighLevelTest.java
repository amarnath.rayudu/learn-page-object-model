package com.hawaiian.test.contactus;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.hawaiian.page.contactus.ContactUsPage;
import com.hawaiian.pages.help.HelpCenterPage;
import com.hawaiian.pages.home.HomePage;
import com.hawaiian.test.base.HawaiianBaseTest;

public class ContactUsHighLevelTest extends HawaiianBaseTest {
	public static final Logger logger = LogManager.getLogger(ContactUsHighLevelTest.class.getName());
	HomePage homePage;
	ContactUsPage contactUsPage;

	@Test(groups = { "sanity", "regression", "contactus", "contactushighLevel" })
	public void verifyHomePageTest() {

		homePage = new HomePage();
		logger.info("Executing verifyHomePageTest");
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
		logger.info("verifyHome page Test is passed");
	}

	@Test(dependsOnMethods = "verifyHomePageTest", groups = { "regression", "contactus", "contactushighLevel" })
	public void navigateToContactUsPageTest() {
		contactUsPage = homePage.navigateToContactUs();
		logger.info("Executing navigate to contactuspageTest");
		assertTrue(contactUsPage.isContactUsPageloaded(), "Contactus page not loaded");
		logger.info("verify navigate to contactuspage is passed");
	}

	@Test(dependsOnMethods = "navigateToContactUsPageTest", groups = { "regression", "contactus",
			"contactushighLevel" })
	public void seatstabTest() {
		assertTrue(contactUsPage.isSeatsTabpresent(), "seats tab is not present");
	}

	@Test(dependsOnMethods = "seatstabTest")
	public void livetabTest() {
		assertTrue(contactUsPage.isLivetabpresent(), "live tab is not present");
	}

	@Test(dependsOnMethods = "seatstabTest", groups = { "regression", "contactus", "contactushighLevel" })
	public void navigateToHomeTest() {
		homePage = contactUsPage.navigateToHome();
		assertTrue(homePage.isHomePageLoaded(), "Home page is not loaded");
	}

}
