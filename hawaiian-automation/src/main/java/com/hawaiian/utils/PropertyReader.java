package com.hawaiian.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class PropertyReader {
	public static final Logger logger = LogManager.getLogger(PropertyReader.class.getName());

	public String getProperty(String key) {

		Properties properties = new Properties();
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream("config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Redaing the porperty for the key -" + key);

		// load a properties file
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Value for the key is " + properties.getProperty(key));

		return properties.getProperty(key);

	}

}