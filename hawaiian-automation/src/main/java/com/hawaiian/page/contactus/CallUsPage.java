package com.hawaiian.page.contactus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.hawaiian.pages.HawaiianBasePage;


public class CallUsPage extends HawaiianBasePage{
	public static final Logger logger = LogManager.getLogger(CallUsPage.class.getName());

	public By calluspageloaded = By.xpath("//div[@class='headline']");
	public By usandcanada = By.xpath("//span[contains(text(),'UNITED STATES')]");
	public By ustollfreeno = By.xpath("//td[contains(text(),'1-800-367-5320')]");
	public By asia = By.xpath("//span[contains(text(),'ASIA')]");
	public By china = By.xpath("//th[contains(text(),'China')]");
	public By europe = By.xpath("//span[contains(text(),'EUROPE')]");
	public By unitedkingdom = By.xpath("//th[contains(text(),'United')]");
	
	
	public boolean isCallUsPageloaded() {
		logger.info("verify isCallUsPageloaded");
		return isElementPresent(calluspageloaded);
		
	}
	public boolean isUSAnsCanadaPresent() {
		logger.info("verifing United States and Canada");
		return isElementPresent(usandcanada);
	}
	public boolean isUsTollFreeNumberPresent() {
		logger.info("Verify Us toll free number");
		driver.findElement(usandcanada).click();
		return isElementPresent(ustollfreeno);
	}
	public boolean isChinaPresent() {
		logger.info("Verifying China ");
		driver.findElement(asia).click();
		return isElementPresent(china);
	}
	public boolean isUnitedKingdomPresent() {
		driver.findElement(europe).click();
		logger.info("Verifying Uk");
		return isElementPresent(unitedkingdom);
	}
	
	

}
