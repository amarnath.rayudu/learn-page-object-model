package com.hawaiian.page.contactus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.hawaiian.pages.HawaiianBasePage;
//aman

public class ContactUsPage extends HawaiianBasePage {
	public By contactusloaded = By.xpath("//div[@class='contact-us page-1']//header//h1[@class='header-text']");
	public By seats = By.xpath(
			"//div[@class='col-12 text-center']//ul//li//a[@href='http://hawaiianair.custhelp.com/app/home/topic/457']");
	public By livetab = By.xpath("//div[@class='col-4']//ul//li//a[@href='/contact-us']");
	public By callUspagetab = By.xpath("//a[contains(text(),'Add')]");
	public static final Logger logger = LogManager.getLogger(ContactUsPage.class.getName());

	public boolean isContactUsPageloaded() {
		logger.info("verifying contactus page loading");
		return isElementPresent(contactusloaded);
	}

	public boolean isSeatsTabpresent() {
		logger.info("verifying seats tab present");
		return isElementPresent(seats);
	}

	public boolean isLivetabpresent() {
		logger.info("verifying Livetab present");
		return isElementPresent(livetab);
	}
	public CallUsPage navigateToCallUsPage() {
		driver.findElement(callUspagetab).click();
		return new CallUsPage();
		
		
	}
}
