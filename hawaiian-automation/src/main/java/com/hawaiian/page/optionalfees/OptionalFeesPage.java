package com.hawaiian.page.optionalfees;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.hawaiian.pages.HawaiianBasePage;


public class OptionalFeesPage extends HawaiianBasePage {
	public By baggage = By.id("Rules");
	public By legal = By.xpath("//div[@class='col-12']//a[@href='/legal']");
	public By kite = By.id("Boards");
	public static final Logger logger = LogManager.getLogger(OptionalFeesPage.class.getName());

	public boolean isbaggageTabPresent() {
		logger.info("verifying baggage tab ");
		return isElementPresent(baggage);
	}
	public boolean islegalTabPresent() {
		logger.info("verifying legal tab");
		return isElementPresent(legal);
	}
	public boolean iskiteTabPresent() {
		logger.info("verifying kite tab");
		return isElementPresent(kite);
	}
	public boolean isOptionalFeesPageLoaded() {
		logger.info("verifying optional fees page is loaded");
		return isElementPresent(baggage);
		
		
	}
}
