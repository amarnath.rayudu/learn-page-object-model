package com.hawaiian.pages.help;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.hawaiian.pages.HawaiianBasePage;

public class HelpCenterPage extends HawaiianBasePage {

	public By AirportInfo = By.id("Summary_450");
	public By HawaiianMiles = By.id("Summary_461");
	public By checkIn = By.id("Summary_464");
	public static final Logger logger = LogManager.getLogger(HelpCenterPage.class.getName());

	public boolean isCheckinLinkPrsent() {
		logger.info(" Verifying checkin center link ");
		return isElementPresent(checkIn);
	}

	public boolean isHelpCenterPageLoaded() {
		logger.info("HELP CENTRE PAGE LOADED ");
		return isElementPresent(AirportInfo);
	}

	public boolean isMilesLinkPresent() {
		logger.info("verify Miles link ");
		return isElementPresent(HawaiianMiles);

	}

}
