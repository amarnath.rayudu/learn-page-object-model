package com.hawaiian.pages.home;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import com.hawaiian.page.contactus.ContactUsPage;
import com.hawaiian.page.optionalfees.OptionalFeesPage;
import com.hawaiian.pages.HawaiianBasePage;
import com.hawaiian.pages.help.HelpCenterPage;

public class HomePage extends HawaiianBasePage {

	public By helpCenterTab = By.xpath("//a[contains(text(),'Help Center')]");
	public By bookingSection = By.id("section-bookflights");
	public By contactustab = By.xpath("//div[@class='col-4']//ul//li//a[@href='/contact-us']");
	public By optionalfeestab=By.xpath("//a[contains(text(),'Optional')]");
	public static final Logger logger = LogManager.getLogger(HomePage.class.getName());
	public boolean isHelpLinkPrsent() {
		logger.info("Verifying Help center link");
	
		return isElementPresent(helpCenterTab);
	}

	
	public boolean isHomePageLoaded() {
		logger.info(" Verifying the homepage loading  ");
		return isElementPresent(bookingSection);
	}

	public HelpCenterPage navigateToHelpCenter() {
		driver.findElement(helpCenterTab).click();
		logger.info(" navigating to help center page  ");
		return new HelpCenterPage();

	}
	public ContactUsPage navigateToContactUs() {
		driver.findElement(contactustab).click();
		logger.info(" navigating to contactus page ");
		return new ContactUsPage();
	}
	public OptionalFeesPage navigateToOptionalFees() {
		driver.findElement(optionalfeestab).click();
		logger.info(" navigating to optional fees page  ");
		return new OptionalFeesPage();
	}
	
}
