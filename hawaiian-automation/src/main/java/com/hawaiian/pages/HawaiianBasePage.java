package com.hawaiian.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.hawaiian.pages.home.HomePage;

public class HawaiianBasePage {
	public By hawaiiLogo = By.xpath("//a[@class='nav-logo text-hide']");
	public static WebDriver driver ;
	public static final Logger logger = LogManager.getLogger(HawaiianBasePage.class.getName());

	public boolean isElementPresent(By locator) {
		logger.info("Verifying the element " + locator);

		return driver.findElements(locator).size() > 0;
	}

	public HomePage navigateToHome() {
		logger.info("Navigating back to Home page");
		driver.findElement(hawaiiLogo).click();
		return new HomePage();

	}

}
